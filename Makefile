
run: all
	./main.native

all:
	ocamlbuild -cflags -I,+lablgtk2 -lflags -I,+lablgtk2 -libs unix,str,lablgtk main.native
	strip --strip-unneeded main.native

clean:
	ocamlbuild -clean

